package com.exambackend.products.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    public long id;
    public String title;
    public double price;
    public String description;
    public String category;
    public String image;
    public Rating rating;
}
