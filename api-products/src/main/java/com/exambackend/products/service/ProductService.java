package com.exambackend.products.service;


import com.exambackend.products.model.Product;
import org.springframework.http.ResponseEntity;

public interface ProductService {
    ResponseEntity getProduct(Long id);
}
