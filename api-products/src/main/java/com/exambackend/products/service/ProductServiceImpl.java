package com.exambackend.products.service;

import com.exambackend.products.model.Product;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ProductServiceImpl implements ProductService {

    private final RestTemplate restTemplate;

    public ProductServiceImpl(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public ResponseEntity getProduct(Long id) {
        String url = String.format("https://fakestoreapi.com/products/%s", id);
        return restTemplate.getForEntity(url, Product.class);
    }

}
