## Examen Practico Backend

  

## Que se necesita para ejecutar las APIs

- JDK 17
- Maven 3.2+
- También puede importar el código directamente a su IDE:

    - Spring Tool Suite (STS)
    - IDEA IntelliJ

- Docker Desktop / Docker / Docker-Compose
- Postman
- Empaquetar los proyectos con maven.
- Desde la terminal de su preferencia ubicarse en la carpeta de descarga
- Ejecutar el comando: **_docker-compose up -d_**
- Abrir Postman e importar la collection **_ExamBackend.postman\_collection.json_**.
- En Postman crear una variable de entorno global con el nombre: **_token_**.

  

En el navegador se puede ver la base de datos con la siguiente información:

URL: [http://localhost:8090/](http://localhost:8090/)

Base de Datos: **shopping\_cart\_db**

Usuario: **userdb**

Password: **db1234**