CREATE TABLE `customers`
(
    `id`      bigint(20) NOT NULL AUTO_INCREMENT,
    `name`    varchar(150) NOT NULL,
    `phone`   varchar(8)   NOT NULL,
    `address` varchar(100) NOT NULL,
    `email`   varchar(100) NOT NULL,
    `active`  tinyint(1) DEFAULT 1,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;

CREATE TABLE `orders`
(
    `id`          bigint(20) NOT NULL AUTO_INCREMENT,
    `ordered`     datetime       NOT NULL,
    `status`      varchar(20)    NOT NULL,
    `customer_id` bigint(20) NOT NULL,
    `total`       decimal(10, 2) NOT NULL,
    PRIMARY KEY (`id`),
    KEY           `orders_customers_fk_idx` (`customer_id`),
    CONSTRAINT `orders_customers_fk` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;

CREATE TABLE `orders_detail`
(
    `id`         bigint(20) NOT NULL AUTO_INCREMENT,
    `order_id`   bigint(20) DEFAULT NULL,
    `product_id` bigint(20) NOT NULL,
    `quantity`   int(11) NOT NULL,
    `price`      decimal(10, 2) NOT NULL,
    PRIMARY KEY (`id`),
    KEY          `orders_detail_orders_fk_idx` (`id`),
    KEY          `orders_detail_orders_fk` (`order_id`),
    CONSTRAINT `orders_detail_orders_fk` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;