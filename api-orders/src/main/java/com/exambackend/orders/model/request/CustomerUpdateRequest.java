package com.exambackend.orders.model.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class CustomerUpdateRequest {

  @NotNull
  private String name;
  @NotNull
  @Pattern(regexp = "^[0-9]{8}$", message = "Not valid value")
  private String phone;
  @NotNull
  private String address;
  @NotNull
  private String email;


}
