package com.exambackend.orders.model.response;

import com.exambackend.orders.model.request.CustomerCreateRequest;

import lombok.Data;

@Data
public class CustomerResponse extends CustomerCreateRequest {
  private Long id;
}
