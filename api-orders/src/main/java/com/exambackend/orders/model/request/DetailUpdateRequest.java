package com.exambackend.orders.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DetailUpdateRequest {

    private Long orderId;
    private Long productId;
    private Integer quantity;
    private BigDecimal price;

}
