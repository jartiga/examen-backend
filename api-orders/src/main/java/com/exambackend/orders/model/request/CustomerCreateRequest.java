package com.exambackend.orders.model.request;

import lombok.Data;

@Data
public class CustomerCreateRequest {

  private String name;
  private String phone;
  private String address;
  private String email;

}
