package com.exambackend.orders.model.response;

import com.exambackend.orders.model.request.DetailCreateRequest;
import lombok.Data;

@Data
public class DetailResponse extends DetailCreateRequest {
  private Long id;
}
