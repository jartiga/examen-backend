package com.exambackend.orders.model.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "orders", catalog = "shopping_cart_db")
public class Order {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", unique = true, nullable = false)
  private Long id;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customer_id", nullable = false)
  private Customer customer;
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "ordered", nullable = false, length = 19)
  private Date ordered;
  @Column(name = "status", nullable = false, length = 20)
  private String status;
  @Column(name = "total", nullable = false, precision = 10)
  private BigDecimal total;
  @OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
  @Cascade(CascadeType.SAVE_UPDATE)
  private List<OrderDetail> ordersDetail = new ArrayList<>();

  public Order(Customer customer, Date ordered, String status, BigDecimal total) {
    this.customer = customer;
    this.ordered = ordered;
    this.status = status;
    this.total = total;
  }


  
}
