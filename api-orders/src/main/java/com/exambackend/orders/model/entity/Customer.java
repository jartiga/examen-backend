package com.exambackend.orders.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "customers", catalog = "shopping_cart_db")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;
    @Column(name = "name", nullable = false, length = 150)
    private String name;
    @Column(name = "phone", nullable = false, length = 8)
    private String phone;
    @Column(name = "address", unique = true, nullable = false, length = 100)
    private String address;
    @Column(name = "email", nullable = false, length = 100)
    private String email;
    @Column(name = "active")
    private boolean active = true;
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
    @Cascade(CascadeType.SAVE_UPDATE)
    private List<Order> orders = new ArrayList<>();

    public Customer(String name, String phone, String address, String email, boolean active) {
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.email = email;
        this.active = active;
    }
}
