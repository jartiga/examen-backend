package com.exambackend.orders.model.request;

import com.exambackend.orders.model.dto.DetailDTO;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class OrderCreateRequest {

  @NotNull
  private Long customerId;
  @NotNull
  private Date ordered;
  @NotNull
  private String status;
  @NotNull
  private BigDecimal total;
  @NotEmpty
  private List<DetailDTO> detail;

}
