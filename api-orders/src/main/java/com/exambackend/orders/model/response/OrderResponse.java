package com.exambackend.orders.model.response;

import com.exambackend.orders.model.request.OrderUpdateRequest;
import lombok.Data;

import java.util.List;

@Data
public class OrderResponse extends OrderUpdateRequest {
  private Long id;
  private List<DetailResponse> detail;
}
