package com.exambackend.orders.model.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class OrderUpdateRequest {

  @NotNull
  private Long customerId;
  @NotNull
  private Date ordered;
  @NotNull
  private String status;
  @NotNull
  private BigDecimal total;

}
