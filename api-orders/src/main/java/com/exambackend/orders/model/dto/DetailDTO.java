package com.exambackend.orders.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DetailDTO {

    private Long productId;
    private Integer quantity;
    private BigDecimal price;

}
