package com.exambackend.orders.order;

import com.exambackend.orders.model.request.OrderCreateRequest;
import com.exambackend.orders.model.request.OrderUpdateRequest;
import com.exambackend.orders.model.response.OrderResponse;
import org.springframework.http.ResponseEntity;

public interface OrderService {

  ResponseEntity getOrderById(Long id);
  ResponseEntity createOrder(OrderCreateRequest orderCreateRequest);
  ResponseEntity updateOrder(Long id, OrderUpdateRequest orderUpdateRequest);

}
