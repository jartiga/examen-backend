package com.exambackend.orders.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.exambackend.orders.model.entity.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
  
}
