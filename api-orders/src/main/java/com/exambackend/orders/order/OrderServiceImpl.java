package com.exambackend.orders.order;

import com.exambackend.orders.customer.CustomerRepository;
import com.exambackend.orders.detail.OrderDetailRepository;
import com.exambackend.orders.model.entity.Customer;
import com.exambackend.orders.model.entity.Order;
import com.exambackend.orders.model.entity.OrderDetail;
import com.exambackend.orders.model.request.OrderCreateRequest;
import com.exambackend.orders.model.request.OrderUpdateRequest;
import com.exambackend.orders.model.response.DetailResponse;
import com.exambackend.orders.model.response.OrderResponse;
import com.exambackend.orders.model.response.MessageResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;

    private final OrderDetailRepository orderDetailRepository;

    public OrderServiceImpl(OrderRepository orderRepository, CustomerRepository customerRepository, OrderDetailRepository orderDetailRepository) {
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
        this.orderDetailRepository = orderDetailRepository;
    }

    @Override
    public ResponseEntity getOrderById(Long id) {
        Optional<Order> order = orderRepository.findById(id);
        if (!order.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("Id does not exist"));
        }
        OrderResponse orderResponse = new OrderResponse();
        BeanUtils.copyProperties(order.get(), orderResponse);

        List<OrderDetail> details = order.get().getOrdersDetail();
        List<DetailResponse> detailResponseList = new ArrayList<>();
        details.stream().forEach(d -> {
            DetailResponse detailResponse = new DetailResponse();
            BeanUtils.copyProperties(d, detailResponse);
            detailResponse.setOrderId(d.getOrder().getId());
            detailResponseList.add(detailResponse);
        });
        orderResponse.setCustomerId(order.get().getCustomer().getId());
        orderResponse.setDetail(detailResponseList);
        return ResponseEntity.ok().body(orderResponse);
    }

    @Override
    public ResponseEntity createOrder(OrderCreateRequest orderCreateRequest) {

        Optional<Customer> customer = customerRepository.findById(orderCreateRequest.getCustomerId());
        if (!customer.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("Customer Id does not exist"));
        }
        OrderResponse orderResponse = new OrderResponse();
        Order order = new Order();
        BeanUtils.copyProperties(orderCreateRequest, order);
        order.setCustomer(customer.get());
        Order orderCreated = orderRepository.save(order);
        List<DetailResponse> detailResponseList = new ArrayList<>();
        orderCreateRequest.getDetail().stream().forEach(detail -> {
            OrderDetail orderDetail = new OrderDetail();
            BeanUtils.copyProperties(detail, orderDetail);
            orderDetail.setOrder(orderCreated);
            OrderDetail detailCreated = orderDetailRepository.save(orderDetail);
            DetailResponse detailResponse = new DetailResponse();
            BeanUtils.copyProperties(detailCreated, detailResponse);
            detailResponseList.add(detailResponse);
        });
        BeanUtils.copyProperties(order, orderResponse);
        orderResponse.setDetail(detailResponseList);
        orderResponse.setCustomerId(customer.get().getId());
        return ResponseEntity.ok().body(orderResponse);
    }

    @Override
    public ResponseEntity updateOrder(Long id, OrderUpdateRequest orderUpdateRequest) {
        Optional<Order> order = orderRepository.findById(id);
        if (!order.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("Id does not exist"));
        }
        Order orderUpd = order.get();
        BeanUtils.copyProperties(orderUpdateRequest, orderUpd);
        orderRepository.save(orderUpd);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

}
