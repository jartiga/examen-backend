package com.exambackend.orders.order;

import com.exambackend.orders.model.request.OrderCreateRequest;
import com.exambackend.orders.model.request.OrderUpdateRequest;
import com.exambackend.orders.model.response.OrderResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/orders")
public class OrderController {
  
  public final OrderService orderService;

  public OrderController(OrderService orderService) {
    this.orderService = orderService;
  }

  @GetMapping("/{id}")
  public ResponseEntity<OrderResponse> getOrderById(@PathVariable("id") Long id) {
    return  orderService.getOrderById(id);
  }

  @PostMapping()
  public ResponseEntity<OrderResponse> createOrder(@Valid @RequestBody OrderCreateRequest orderCreateRequest){
    return orderService.createOrder(orderCreateRequest);
  }

  @PutMapping("/{id}")
  public ResponseEntity updateOrder(@PathVariable("id") Long id, @RequestBody OrderUpdateRequest orderUpdateRequest) {
    return orderService.updateOrder(id, orderUpdateRequest);
  }

}
