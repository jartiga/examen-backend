package com.exambackend.orders.filter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class JwtTokenAuthenticationFilter extends OncePerRequestFilter {

    private static final String AUTHORITIES_KEY = "authorities";
    private final String PREFIX = "Bearer ";
    private String SECRET="pruebaKey";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        String header = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (header == null) {
            chain.doFilter(request, response);
            System.out.println("RETURN 1");
            return;
        }

        String token = header.replace(PREFIX, "");

        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token)
                    .getBody();
            String username = claims.getSubject();
            if (username != null) {
                @SuppressWarnings("unchecked")
                List<String> authorities = (List<String>) claims.get(AUTHORITIES_KEY);
                if (null == authorities || authorities.size() == 0) {
                    authorities = new ArrayList<String>();
                    authorities.add("ROLE_USER");
                }
                UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                        username, null, authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));

                SecurityContextHolder.getContext().setAuthentication(auth);

                HeaderMapRequestWrapper requestWrapper = new HeaderMapRequestWrapper(request);

                requestWrapper.addHeader("ROLE", String.join("-", authorities));
                requestWrapper.addHeader("USER", username);

                chain.doFilter(requestWrapper, response);
            } else {
                chain.doFilter(request, response);
            }

        } catch (Exception e) {
            SecurityContextHolder.clearContext();
            chain.doFilter(request, response);
        }

    }

}
