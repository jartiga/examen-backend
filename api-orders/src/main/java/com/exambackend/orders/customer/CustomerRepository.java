package com.exambackend.orders.customer;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exambackend.orders.model.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
  
}
