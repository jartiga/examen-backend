package com.exambackend.orders.customer;

import java.util.Optional;

import com.exambackend.orders.model.request.CustomerCreateRequest;
import com.exambackend.orders.model.response.MessageResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.exambackend.orders.model.entity.Customer;
import com.exambackend.orders.model.request.CustomerUpdateRequest;
import com.exambackend.orders.model.response.CustomerResponse;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

  private final CustomerRepository customerRepository;

  public CustomerServiceImpl(CustomerRepository customerRepository) {
    this.customerRepository = customerRepository;
  }
  
  @Override
  public ResponseEntity getCustomerById(Long id) {
    Optional<Customer> customer = customerRepository.findById(id);
    if (!customer.isPresent()) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("Id does not exist"));
    }
    CustomerResponse response = new CustomerResponse();
    BeanUtils.copyProperties(customer.get(), response);
    return ResponseEntity.ok().body(response);
  }

  public ResponseEntity createCustormer(CustomerCreateRequest customerCreateRequest) {
    CustomerResponse response = new CustomerResponse();
    Customer customer = new Customer();
    BeanUtils.copyProperties(customerCreateRequest, customer);
    customer = customerRepository.save(customer);
    BeanUtils.copyProperties(customer, response);
    return ResponseEntity.ok().body(response);
  }

  @Override
  public ResponseEntity updateCustomer(Long id, CustomerUpdateRequest customerUpdateRequest) {
    Optional<Customer> customer = customerRepository.findById(id);
    if (!customer.isPresent()) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("Id does not exist"));
    }
    Customer customerUpd = customer.get();
    BeanUtils.copyProperties(customerUpdateRequest, customerUpd);
    customerRepository.save(customerUpd);
    return ResponseEntity.status(HttpStatus.CREATED).build();
  }

}
