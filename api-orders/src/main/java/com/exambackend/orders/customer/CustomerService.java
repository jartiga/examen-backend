package com.exambackend.orders.customer;

import com.exambackend.orders.model.request.CustomerCreateRequest;
import com.exambackend.orders.model.request.CustomerUpdateRequest;
import com.exambackend.orders.model.response.CustomerResponse;
import org.springframework.http.ResponseEntity;

public interface CustomerService {

  ResponseEntity getCustomerById(Long id);
  ResponseEntity createCustormer(CustomerCreateRequest customerCreateRequest);
  ResponseEntity updateCustomer(Long id, CustomerUpdateRequest customerUpdateRequest);

}
