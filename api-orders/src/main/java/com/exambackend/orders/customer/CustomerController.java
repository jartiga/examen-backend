package com.exambackend.orders.customer;

import com.exambackend.orders.model.request.CustomerCreateRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.exambackend.orders.model.request.CustomerUpdateRequest;
import com.exambackend.orders.model.response.CustomerResponse;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
  
  public final CustomerService customerService;

  public CustomerController(CustomerService customerService) {
    this.customerService = customerService;
  }

  @GetMapping("/{id}")
  public ResponseEntity<CustomerResponse> getCustomerById(@PathVariable("id") Long id) {
    return customerService.getCustomerById(id);
  }

  @PostMapping()
  public ResponseEntity<CustomerResponse> createCustomer(@Valid @RequestBody CustomerCreateRequest customerCreateRequest){
    return customerService.createCustormer(customerCreateRequest);
  }

  @PutMapping("/{id}")
  public ResponseEntity updateCustomer(@PathVariable("id") Long id, @Valid @RequestBody CustomerUpdateRequest customerUpdateRequest) {
    return customerService.updateCustomer(id, customerUpdateRequest);
  }


}
