package com.exambackend.orders.detail;

import com.exambackend.orders.model.request.DetailCreateRequest;
import com.exambackend.orders.model.request.DetailUpdateRequest;
import com.exambackend.orders.model.response.DetailResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/details")
public class OrderDetailController {
  
  public final OrderDetailService orderDetailService;

  public OrderDetailController(OrderDetailService orderDetailService) {
    this.orderDetailService = orderDetailService;
  }

  @GetMapping("/{id}")
  public ResponseEntity<DetailResponse> getCustomerById(@PathVariable("id") Long id) {
    DetailResponse detailResponse = orderDetailService.getOrderDetailById(id);
    return ResponseEntity.ok().body(detailResponse);
  }

  @PostMapping()
  public ResponseEntity<DetailResponse> createCustomer(@Valid @RequestBody DetailCreateRequest detailCreateRequest){
    return orderDetailService.createOrderDetail(detailCreateRequest);
  }

  @PutMapping("/{id}")
  public ResponseEntity updateCustomer(@PathVariable("id") Long id, @Valid @RequestBody DetailUpdateRequest detailUpdateRequest) {
    return orderDetailService.updateOrderDetail(id, detailUpdateRequest);
  }

}
