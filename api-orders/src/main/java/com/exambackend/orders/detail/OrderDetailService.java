package com.exambackend.orders.detail;

import com.exambackend.orders.model.request.DetailCreateRequest;
import com.exambackend.orders.model.request.DetailUpdateRequest;
import com.exambackend.orders.model.response.DetailResponse;
import org.springframework.http.ResponseEntity;

public interface OrderDetailService {
  
  DetailResponse getOrderDetailById(Long id);
  ResponseEntity createOrderDetail(DetailCreateRequest orderCreateRequest);
  ResponseEntity updateOrderDetail(Long id, DetailUpdateRequest orderUpdateRequest);

}
