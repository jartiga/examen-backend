package com.exambackend.orders.detail;

import com.exambackend.orders.model.entity.Order;
import com.exambackend.orders.model.entity.OrderDetail;
import com.exambackend.orders.model.request.DetailCreateRequest;
import com.exambackend.orders.model.request.DetailUpdateRequest;
import com.exambackend.orders.model.response.DetailResponse;
import com.exambackend.orders.model.response.MessageResponse;
import com.exambackend.orders.order.OrderRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class OrderDetailServiceImpl implements OrderDetailService {

  private final OrderDetailRepository orderDetailRepository;
  private final OrderRepository orderRepository;

  public OrderDetailServiceImpl(OrderDetailRepository orderDetailRepository, OrderRepository orderRepository) {
    this.orderDetailRepository = orderDetailRepository;
    this.orderRepository = orderRepository;
  }


  @Override
  public DetailResponse getOrderDetailById(Long id) {
    DetailResponse detailResponse = new DetailResponse();
    Optional<OrderDetail> orderDetail = orderDetailRepository.findById(id);
    if (orderDetail.isPresent()) {
      BeanUtils.copyProperties(orderDetail.get(), detailResponse);
    }
    return detailResponse;
  }

  @Override
  public ResponseEntity createOrderDetail(DetailCreateRequest detailCreateRequest) {
    Optional<Order> order = orderRepository.findById(detailCreateRequest.getOrderId());
    if (!order.isPresent()){
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("Order Id does not exist"));
    }

    DetailResponse detailResponse = new DetailResponse();
    OrderDetail orderDetail = new OrderDetail();
    BeanUtils.copyProperties(detailCreateRequest, orderDetail);
    orderDetail.setOrder(order.get());
    OrderDetail orderDetailCreated = orderDetailRepository.save(orderDetail);
    BeanUtils.copyProperties(orderDetailCreated, detailResponse);
    detailResponse.setOrderId(order.get().getId());
    return ResponseEntity.ok().body(detailResponse);
  }

  @Override
  public ResponseEntity updateOrderDetail(Long id, DetailUpdateRequest detailUpdateRequest) {
    Optional<OrderDetail> orderDetail = orderDetailRepository.findById(id);
    if (!orderDetail.isPresent()) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("Id does not exist"));
    }
    OrderDetail detail = orderDetail.get();
    BeanUtils.copyProperties(detailUpdateRequest, detail);
    orderDetailRepository.save(detail);
    return ResponseEntity.status(HttpStatus.CREATED).build();
  }

}
