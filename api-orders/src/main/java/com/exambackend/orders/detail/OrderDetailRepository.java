package com.exambackend.orders.detail;

import com.exambackend.orders.model.entity.Order;
import com.exambackend.orders.model.entity.OrderDetail;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderDetailRepository extends CrudRepository<OrderDetail, Long> {

    List<OrderDetail> findOrderDetailsByOrder(Order order);
  
}
