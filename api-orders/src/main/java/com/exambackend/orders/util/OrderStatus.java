package com.exambackend.orders.util;

public enum OrderStatus {
    NEW, HOLD, SHIPPED, DELIVERED, CLOSED;
}
