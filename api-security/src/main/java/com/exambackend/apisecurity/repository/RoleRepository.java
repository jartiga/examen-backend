package com.exambackend.apisecurity.repository;

import com.exambackend.apisecurity.model.entity.ERole;
import com.exambackend.apisecurity.model.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository  extends JpaRepository<Role, Integer> {
    Optional<Role> findByName(ERole name);
}