package com.exambackend.apisecurity.model.entity;

public enum ERole {
  ROLE_USER,
  ROLE_MODERATOR,
  ROLE_ADMIN,
  ROLE_CLIENT
}
