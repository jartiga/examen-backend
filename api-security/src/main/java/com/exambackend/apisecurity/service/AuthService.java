package com.exambackend.apisecurity.service;

import com.exambackend.apisecurity.model.request.LoginRequest;
import com.exambackend.apisecurity.model.request.SignupRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


public interface AuthService {

    ResponseEntity<?> authenticateUser(LoginRequest loginRequest);
    ResponseEntity<?> registerUser(SignupRequest signUpRequest);
}
